#pragma once

typedef size_t  state_id_t;       
typedef size_t  state_code_t;
typedef void*   state_user_data_t;  
typedef bool(*  state_step_func)(state_user_data_t&, state_id_t&,state_code_t&);


state_code_t state_tick(
    state_id_t&         id, 
    state_user_data_t   ud,
    state_step_func     func
);