#include "state.h"

state_code_t state_tick(
    state_id_t&         id, 
    state_user_data_t   ud,
    state_step_func     func
) {
    state_code_t code = 0;
    
    while(func(ud, id_ref, code));
    
    return code;
}